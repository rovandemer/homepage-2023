// Importing the gallery.json file with web requests
fetch('./data/gallery.json')
    .then(response => response.json())
    .then(data => {
        const carousel = document.getElementById("carousel-container");
        
        // Looping through the galleries
        data.forEach((gallery, index) => {

            // We create a section with class carousel-item
            const section = document.createElement("section");
            section.classList.add("carousel-item");

            // We create an image with class carousel-item-img
            const img = document.createElement("img");
            img.classList.add("carousel-item-img");
            img.src = `${gallery.url}${gallery.mainImg}`;

            // We create an H1 with class carousel-item-title
            const title = document.createElement("h1");
            title.classList.add("carousel-item-title");
            title.textContent = gallery.name;

            // We create a P with class carousel-item-description
            const description = document.createElement("p");
            description.classList.add("carousel-item-description");
            description.textContent = gallery.description;

            // We add the image, title and description to the section 
            section.appendChild(img);
            section.appendChild(title);
            section.appendChild(description);

            // We add the section to the carousel
            carousel.appendChild(section);

        });

    });


