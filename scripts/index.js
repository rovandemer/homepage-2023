/*

Going to generate all the projects from the projects.json file.

*/

// Importing the projects.json file with web requests
fetch('./data/projects.json')
    .then(response => response.json())
    .then(data => {
        // Getting the projects container
        let projectContainer = document.getElementById('projects-container');
        // Looping through the projects
        for (let i = 0; i < data.projects.length; i++) {
            
            const project = data.projects[i];

            // Creating the project article
            let projectArticle = document.createElement('article');
            projectArticle.classList.add('project');
            projectArticle.id = `project-${i}`;
            
            // Set to the id of the project a background image
            projectArticle.style.backgroundImage = `url(${project.image})`;

            // Creating project content div
            let projectContent = document.createElement('div');
            projectContent.classList.add('project-content');

            // If text color is set, set it
            if (project.textColor) {
                projectContent.style.color = project.textColor;
            }

            // Adding the project title
            let projectTitle = document.createElement('h2');
            projectTitle.classList.add('project-title');
            projectTitle.innerHTML = project.name;
            projectContent.appendChild(projectTitle);

            // Adding the project description
            let projectDescription = document.createElement('p');
            projectDescription.classList.add('project-description');
            projectDescription.innerHTML = project.description;
            projectContent.appendChild(projectDescription);

            // Adding the project content to the project article
            projectArticle.appendChild(projectContent);

            // Adding the project link
            projectArticle.onclick = () => {
                // window.location.href = `./project.html?project=${i}`;
                // Relocate to construction page
                window.location.href = `./construction.html`;
            }

            // Adding the project article to the container
            projectContainer.appendChild(projectArticle);

        }

    });

// Import gallery
var IMAGE_NUMBER = 10;
for (var i = 1; i <= IMAGE_NUMBER; i++) {
    var image = document.createElement('img');
    image.src = `./img/gallery/index/${i}.jpg`;
    image.alt = `Image ${i}`;
    image.classList.add('gallery-item');
    // Set opacity to 0
    image.style.opacity = 0;
    document.getElementById('gallery-container').appendChild(image);
}