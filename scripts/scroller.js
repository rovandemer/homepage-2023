var controller = new ScrollMagic.Controller();

// Increase opacity of the text as the user scrolls down
// Add this to each .gallery-item
var galleryItem = document.getElementsByClassName('gallery-item');

const addFade = (element) => {

    var elementTl = new TimelineMax()
        .fromTo(element, 1, { opacity: 0 }, { opacity: 1 });

    var scene = new ScrollMagic.Scene({
        triggerElement: element,
        triggerHook: 0.7,
        reverse: true,
        offset: -100,
        duration: "800vh"
    })
        // Add effect opacity
        .setTween(elementTl)
        .addTo(controller);
    
}

// Get projects
var projects = document.getElementById('projects');

// Add fade effect
addFade(projects);

for (var i = 0; i < galleryItem.length; i++) {
    
    // Add fade effect
    addFade(galleryItem[i]);
    
}