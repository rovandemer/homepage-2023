# For each image, scale it down by 25% and save it
import os
from PIL import Image

# Get the current working directory
cwd = os.getcwd()

# Get the list of files in the current directory
files = os.listdir(cwd)

# Loop through each file
for file in files:
    # Check if the file is an image
    if file.endswith(".jpg"):
        # Open the image
        img = Image.open(file)
        # Get the image size
        width, height = img.size
        # Resize the image
        resized = img.resize((int(3 * width /4), int(3 * height/4)))
        # Save the resized image
        resized.save(file)