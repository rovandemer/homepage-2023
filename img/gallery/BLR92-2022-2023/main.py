# For each image, scale it down by 25% and save it
import os
from PIL import Image

# Get the current working directory
cwd = os.getcwd()

# Get the list of files in the current directory
files = os.listdir(cwd)

# Loop through each file
n = 1
for file in files:
    # Check if the file is an image (webp, jpg, jpeg, png)
    if file.endswith(".webp") or file.endswith(".jpg") or file.endswith(".jpeg") or file.endswith(".png"):
        # Open the image
        img = Image.open(file)
        # Get the width and height of the image
        width, height = img.size
        # Scale the image down by 25%
        img = img.resize((int(3*width/4), int(3*height/4)))
        # Save the image
        img.save(file)
        # Rename the image with n
        os.rename(file, str(n) + ".jpg")

        # Increment n
        n += 1