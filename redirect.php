<?php

// On ajoute un cookie pour savoir si on a déjà été redirigé (cookie de 10 jours)
if (!isset($_COOKIE['redirected'])) {
    setcookie('redirected', '1', time() + 864000);

    // On ouvre le fichier compteur.txt, on incrémente sa valeur de la date et l'heure actuelle puis on le ferme
    $file = fopen('compteur.txt', 'r+');
    // On va à la fin du fichier
    fseek($file, 0, SEEK_END);
    // On écrit la date et l'heure actuelle
    fputs($file, date('d/m/Y H:i:s'));
    // On ajoute une ligne vide
    fputs($file, '
');
    fclose($file);
}

// Attends qu'on aie bien écrit dans le fichier
sleep(.3);

?>

<!DOCTYPE HTML>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="0; url=https://www.youtube.com/watch?v=xvFZjo5PgG0">
    <script type="text/javascript">
        window.location.href = "https://www.youtube.com/watch?v=xvFZjo5PgG0"
    </script>
    <title>Page Redirection</title>
</head>

<body>
</body>

</html>
